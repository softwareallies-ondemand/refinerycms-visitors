## How to install on a refinerycms app

first of all, add the gem to the gemfile

	gem 'refinerycms-visitors', '1.0.1',git: 'https://bitbucket.org/softwareallies-ondemand/refinerycms-visitors.git'

then proceed with:

	bundle install
	rails generate refinery:visitors
	rake db:migrate
	rake db:seed

next you can run the command rake routes in order to see available routes

	rake routes

in order to verify if a visitor is logged in you could use something like this in your ApplicationController

	class ApplicationController < ActionController::Base
	  before_filter :only_visitors

	  def only_visitors
	  	begin
		  	if page.only_logged_visitor && !current_visitor.present?

		  		redirect_to "/", alert: "401, Not authorized!"
		  	end
		rescue
		end
	  end
	end

current_visitor helper will be present on your views too, so all you need is use it. i.e.

	<div class="col-lg-5 col-md-3 col-sm-3 col-xs-2">
	    <%if current_visitor.present? %>
	        <b>Hi <%=current_visitor.email%></b> <button onclick="document.location='/visitors/sign_out'" class=" btn-primary-login btn-login btn-sm btn-responsive pull-right">LogOut?</button>
	    <%else%>
	        <button onclick="document.location='/visitors/sign_in'" class=" btn-primary-login btn-login btn-sm btn-responsive pull-right">Login</button>
	    <%end%>
	</div>


Enjoy it!.

#---------------------------------

## Additionally, to build the source as a gem

    cd vendor/extensions/visitors
    gem build refinerycms-visitors.gemspec
    gem install refinerycms-visitors.gem

