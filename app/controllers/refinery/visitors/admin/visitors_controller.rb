module Refinery
  module Visitors
    module Admin
      class VisitorsController < ::Refinery::AdminController

        crudify :'refinery/visitors/visitor'


        def create
           @visitor = Refinery::Visitors::Visitor.new(visitor_params)
           
           if @visitor.save
            redirect_to "/refinery/visitors", info: "Visitor Succesfuly saved"
           else
            render 'new'
           end
        end

        def update
          @visitor = Refinery::Visitors::Visitor.find(params[:id])
         
          if @visitor.update(visitor_params)
            redirect_to "/refinery/visitors", info: "Visitor Succesfuly updated"
          else
            render 'edit'
          end
        end

        def destroy
          @visitor = Refinery::Visitors::Visitor.find(params[:id])
          @visitor.destroy
         
          redirect_to "/refinery/visitors", info: "Visitor Succesfuly destroyed"
        end

        private

        # Only allow a trusted parameter "white list" through.
        def visitor_params
          params.require(:visitor).permit(:email, :name, :password, :password_confirmation)
        end
      end
    end
  end
end
