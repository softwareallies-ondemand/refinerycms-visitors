
class CreateRefinerycmsVisitorsSchema < ActiveRecord::Migration

  def up
   create_table :refinery_visitors do |t|
      t.string :email
      t.string :name

      ## Database authenticatable
      t.string :encrypted_password, :null => false, :default => ""

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      t.integer :position 

      t.timestamps
    end
  end

  def down
   if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-visitors"})
    end

    #if defined?(::Refinery::Page)
    #  ::Refinery::Page.delete_all({:link_url => "/visitors/visitors"})
    #end

    drop_table :refinery_visitors
  end

end