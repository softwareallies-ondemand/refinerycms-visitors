class AddOnlyLoggedVisitorToRefineryPages < ActiveRecord::Migration
  def change
    add_column :refinery_pages, :only_logged_visitor, :boolean
  end
  def down
  	remove_column :refinery_pages, :only_logged_visitor
  end
end
