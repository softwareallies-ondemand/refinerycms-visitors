Refinery::Core::Engine.routes.draw do

  # Frontend routes
  devise_for :visitors, 
    :class_name => "Refinery::Visitors::Visitor", 
    :controllers => {
        :sessions => 'refinery/visitors/sessions', 
    }


  # Admin routes
  namespace :visitors, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :visitors, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
