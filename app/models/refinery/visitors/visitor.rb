module Refinery
  module Visitors
    class Visitor < Refinery::Core::BaseModel

      self.table_name = 'refinery_visitors'      

      acts_as_indexed :fields => [:name]
      validates :email, :presence => true, :uniqueness => true

      #devise methods
      devise :database_authenticatable, :rememberable, :trackable,:validatable, :authentication_keys => [:email]


    end
  end
end