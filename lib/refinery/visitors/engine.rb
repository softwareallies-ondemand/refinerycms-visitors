module Refinery
  module Visitors
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Visitors

      engine_name :refinery_visitors

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "visitors"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.visitors_admin_visitors_path }
          plugin.pathname = root
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Visitors)
      end
    end
  end
end
